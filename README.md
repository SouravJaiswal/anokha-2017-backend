# Anokha 2017 - API System

Node based APIs system for Anokha 2017.

## Getting Started

* Fork a copy of this repo and clone that on your local machine.

* After cloning, `cd` inside the folder and do `npm install`. 

* The above step will download a lot of stuff, so be patient. Once its done, startup the server with `npm start`.
 
* There will a output on the terminal that the server is up and running.

* Go to a browser and check if it the server is up. 
 



### Prerequisites

* Mongodb

* Node

* npm

* nodemon

* Google chrome

* Postman

* and a proper IDE.



### Installing

* Install mongodb and node on your systems.
 
* Check if npm is installed by `npm --version`. 
 
* Install nodemon using `npm install -g nodemon`. Depending upon your system, you will may have to run the command with sudo.

* Postman is a plugin for google chrome. Install the latest google chrome and then install Postman.

* I would suggest to use Webstorm for a fast and smooth development experience.


